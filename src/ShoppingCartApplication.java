import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShoppingCartApplication {

    //Defining variables we need
    private String name;
    private double priceOfItem;
    private double appleDiscount;
    private double orangeDiscount;

    //Creating constructor
    public ShoppingCartApplication(){}
    public ShoppingCartApplication(String name, double priceOfItem){
        this.name = name;
        this.priceOfItem = priceOfItem;
    }

    //Creating getters and setters

    //For name
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    //For price
    public double getPriceOfItem(){
        return priceOfItem;
    }
    public void setPriceOfItem(double priceOfItem){
        this.priceOfItem = priceOfItem;
    }
    public double getAppleDiscount() {
        return appleDiscount;
    }

    public double getOrangeDiscount() {
        return orangeDiscount;
    }

    public void setAppleDiscount(double appleDiscount) {
        this.appleDiscount = appleDiscount;
    }

    public void setOrangeDiscount(double orangeDiscount) {
        this.orangeDiscount = orangeDiscount;
    }

    //Array to keep track of items in our cart
    ArrayList<ShoppingCartApplication> shoppingCart = new ArrayList<ShoppingCartApplication>();

    //Add to cart method
    public void addItem(String item, double price){
        //add to the shoppingCart array a new item with the values item and price that are passed to the method.
        shoppingCart.add(new ShoppingCartApplication(item,price));
        System.out.println("Added" +item+ "to cart ");
    }
    public void removeItem(String item) {
        int itemPosition = -1;
        for (int i = 0; i < shoppingCart.size(); i++) {
            if (shoppingCart.get(i).name.toLowerCase().equals(item.toLowerCase())) {
                itemPosition = i;
            }
        }
        if (itemPosition != -1) {
            System.out.println("Removed " + item + " from cart");
            shoppingCart.remove(itemPosition);
        }
    }

    //Print Receipt method
    public void printReceipt(){
        System.out.println("Receipt");
        /*
        The below code is called an enhanced for loop or for-each loop
        It a enhancded for loop and is used specificaly when dealing with arrays
         */
        for (ShoppingCartApplication elements : shoppingCart){
            System.out.println(elements.getName() + "..................." + elements.getPriceOfItem());
        }
        //Note we call the method below, in the printReciept method. This can be done inside the same class.
        totalToPay();
    }
    //Creating total cost method
    public void totalToPay(){
        //declaring the variable
        double totalCost = 0;
        double discountCost = 0;
        //enhanched for loop to itterate through the elements in the array just like in the printReciept method
        for (ShoppingCartApplication elements : shoppingCart){
            discount();
            totalCost += elements.getPriceOfItem();
            discountCost = (totalCost - getAppleDiscount() - getOrangeDiscount());
        }
        System.out.println("The total cost of your shop is: " + totalCost);
        System.out.println("The cost of your shop after discounts have been applied: " + discountCost);
        System.out.println("You have saved: " +(totalCost - discountCost)+ " on this shop");
    }
    public void discount(){

        int appleCount = 0;
        for (ShoppingCartApplication item : shoppingCart){
            if (item.getName().equals("Apple")){
                appleCount ++;
            }
        }
        int orangeCount = 0;
        for (ShoppingCartApplication item : shoppingCart){
            if (item.getName().equals("Orange")){
                orangeCount ++;
            }
        }
        //If you buy three apples, you get 10p back.
        if(appleCount >= 3){
            setAppleDiscount(10);
        }
        //if you buy 2 oranges, you get 20p back
        if(orangeCount >= 2){
            setOrangeDiscount(20);
        }
    }
}
