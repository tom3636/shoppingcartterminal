//tutorial video https://www.youtube.com/watch?v=VS1DfbpL5LE

import java.util.Scanner;

public class Main {
    public static void main(String []args){

        ShoppingCartApplication item = new ShoppingCartApplication();


        System.out.println("Welcome to my store");
        Scanner in = new Scanner(System.in);
        System.out.println("1 Adding item \n2 Remove item \n3 Reciept\n4 Exit ");
        System.out.println("Your input: ");
        int input = in.nextInt();

        do {
            if (input == 1) {
                //System.out.println("Adding item");
                Scanner itemNameInput = new Scanner(System.in);
                System.out.print("Input item name: ");
                String itemName = itemNameInput.nextLine();

                Scanner itemPriceInput = new Scanner(System.in);
                System.out.print("Input item price: ");
                double itemPrice = itemPriceInput.nextDouble();

                item.addItem(itemName, itemPrice);

                System.out.println("1 Adding item \n2 Remove item \n3 Reciept\n4 Exit ");
                System.out.println("Your input: ");
                input = in.nextInt();
            } else if (input == 2) {
                //System.out.println("Removing item");
                Scanner removing = new Scanner(System.in);
                System.out.println("Input item name: ");
                String name = removing.nextLine();
                item.removeItem(name);

                System.out.println("1 Adding item \n2 Remove item \n3 Reciept\n4 Exit ");
                System.out.println("Your input: ");
                input = in.nextInt();

            } else if (input == 3) {
                item.printReceipt();
                break;
            } else if (input == 4) {
                System.out.println("Exit");
            }

        }while(input>0 && input<4);

    }
}
